import { Pool } from "pg";
import dotenv from 'dotenv';
dotenv.config();
import { v4 as uuidv4 } from 'uuid';

export type User = {
    id: string
    name: string
    email: string
}

export type Tournament= {
    tournament_id: string,
    name: string
    owner_id: string
    win_points: number
    tie_points: number
    defeat_points: number
}

export type Player = {
    player_id: string
    player_name: string
    tournament_id: string
    player_number: number
}

export type Game = {
    game_id: string
    player1_id: string
    player2_id: string
    round: number
    game_order_number: number
    score1: number
    score2: number
    tournament_id: string
}

export type GameSchedule = {
    order_number: number
    round: number
    player1_number: number
    player2_number: number
}

export type StandingsEntry = {
    player: Player
    wins: number
    ties: number
    defeats: number
    played: number
    points: number
}

const pool = new Pool ({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: 5432,
    //ssl: true
})

export async function getUserIds() {
    const user_ids: string[] = [];
    const results = await pool.query('SELECT id, name, email FROM users;');
    results.rows.forEach(r => {
        user_ids.push(r["id"])
    });

    return user_ids;
}

export async function insertUser(user_id: string, name: string, email: string) {
    await pool.query('INSERT INTO users VALUES ($1, $2, $3);', [user_id, name, email]);
}

export async function insertNewTournament(tournament_id: string, name: string, user_id: string, win_points: number, tie_points: number, defeat_points: number) {

    await pool.query('INSERT INTO tournaments_info VALUES ($1, $2, $3, $4, $5, $6);', [tournament_id, name, user_id, win_points, tie_points, defeat_points]);
}

export async function insertPlayers(player_names: string[], tournament_id: string) {

    player_names.forEach(async (player_name: string, index: number) => {

        let player_id = uuidv4();

        await pool.query('INSERT INTO players VALUES ($1, $2, $3, $4);', [player_id, player_name, tournament_id, index + 1]);
    })
}

export async function getUsersTournaments(user_id: string) {
    const tournaments: Tournament[] = [];
    const results = await pool.query('SELECT * FROM tournaments_info WHERE owner_id=$1', [user_id]);
    results.rows.forEach(r => {
        tournaments.push({
            tournament_id: r["tournament_id"],
            name: r["name"],
            owner_id: r["owner_id"],
            win_points: r["win_points"],
            tie_points: r["tie_points"],
            defeat_points: r["defeat_points"]
        });
    })

    return tournaments
}

export async function getTournamentInfo(tournament_id: string) {
    let tournament: Tournament;
    const result = await pool.query('SELECT * FROM tournaments_info WHERE tournament_id=$1', [tournament_id]);
    const r = result.rows[0]

    if(r === undefined) {
        throw new Error('No data for given tournament id')
    }

    tournament = {
        tournament_id: r["tournament_id"],
        name: r["name"],
        owner_id: r["owner_id"],
        win_points: r["win_points"],
        tie_points: r["tie_points"],
        defeat_points: r["defeat_points"]
    };

    return tournament
}

export async function getTournamentPlayers(tournament_id: string) {
    const players: Player[] = [];

    const results = await pool.query('SELECT * FROM players WHERE tournament_id=$1 ORDER BY player_number ASC', [tournament_id]);
    results.rows.forEach(r => {
        players.push({
            player_id: r["player_id"],
            player_name: r["player_name"],
            tournament_id: r["tournament_id"],
            player_number: r["player_number"]
        })
    })

    return players
}

export async function getTournamentGames(tournament_id: string) {
    const games: Game[] = [];

    const results = await pool.query('SELECT * FROM games WHERE tournament_id=$1 ORDER BY game_order_number ASC', [tournament_id]);
    results.rows.forEach(r => {
        games.push({
            game_id: r["game_id"],
            player1_id: r["player1_id"],
            player2_id: r["player2_id"],
            round: r["round"],
            game_order_number: r["game_order_number"],
            score1: r["score1"],
            score2: r["score2"],
            tournament_id: r["tournament_id"]
        })
    })

    return games;
}

export async function updateGameScore(game_id: string, score1: number, score2: number) {
    await pool.query('UPDATE games SET score1=$1, score2=$2 WHERE game_id=$3', [score1, score2, game_id]);
}

export async function insertGame(tournament_id: string, player1_id: string, player2_id: string, round: number, game_order_number: number, score1: number, score2: number) {

    const game_id = uuidv4();

    await pool.query('INSERT INTO games VALUES($1, $2, $3, $4, $5, $6, $7, $8)', [game_id, player1_id, player2_id, round, game_order_number, score1, score2, tournament_id]);
}

export async function deleteGame(game_id: string) {
    await pool.query('DELETE FROM games WHERE game_id=$1', [game_id]);
    return
}

export async function getPlayerGames(player_id: string) {
    const games: Game[] = [];
    const results = await pool.query('SELECT * FROM games WHERE player1_id=$1 OR player2_id=$2', [player_id, player_id]);
    results.rows.forEach(r => {
        games.push({
            game_id: r["game_id"],
            player1_id: r["player1_id"],
            player2_id: r["player2_id"],
            round: r["round"],
            game_order_number: r["game_order_number"],
            score1: r["score1"],
            score2: r["score2"],
            tournament_id: r["tournament_id"]
        })
    })

    return games;
}

export async function getTournamentSchedule(contestants_number: number) {
    let tableName: string;

    switch(contestants_number) {
        case 4:
           tableName = 'pairing4';
           break;
        case 5:
            tableName = 'pairing5';
            break;
        case 6:
            tableName = 'pairing6';
            break;
        case 7:
            tableName = 'pairing7';
            break;
        default: // 8
            tableName = 'pairing8';
            break; 
    }

    const query = `SELECT * FROM ${tableName} ORDER BY order_number ASC`;

    const gameSchedules: GameSchedule[] = [];

    const results = await pool.query(query);
    results.rows.forEach(r => {
        gameSchedules.push({
            order_number: r["order_number"],
            round: r["round"],
            player1_number: r["player1_number"],
            player2_number: r["player2_number"]
        })
    })

    return gameSchedules;
}