import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import fs from 'fs';
import path from 'path';
import https from 'https';
import { auth, requiresAuth } from 'express-openid-connect';
import * as db from './db';
import { v4 as uuidv4 } from 'uuid';

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.static('public')); // check if this should be used
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

console.log(process.env.SECRET)

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.SECRET,
    baseURL: externalUrl || `https://localhost:${port}`,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: 'https://dev-idydexc5vo5wc0yw.us.auth0.com',
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
        response_type: 'code' ,
        scope: "openid profile email"   
       },
  };

// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));

app.get('/', async function(req, res) {

    let username: string | undefined;

    if(req.oidc.isAuthenticated()) {
        username = req.oidc.user?.name ?? req.oidc.user?.sub
    }

    try {
        res.render('index', {username});

    } catch(err) {
        console.error(err)
        res.status(500).send('Internal Server Error')
    }
});

app.get('/login', function(req, res) {
    res.oidc.login({
        returnTo: '/',
        authorizationParams: {
            screen_hint: "login"
        }
    });
});

app.get("/sign-up", (req, res) => {
    res.oidc.login({
      returnTo: '/',
      authorizationParams: {      
        screen_hint: "signup",
      },
    });
  });

app.get("/my-tournaments", requiresAuth(), async function (req, res) {

    const user_id: string = req.oidc.user?.sub;

    const tournaments = await db.getUsersTournaments(user_id);

    res.render('my-tournaments', {tournaments})
})

app.post("/my-tournaments", requiresAuth(), async function(req, res) {
    const name: string = req.body.name;
    const contestants_text: string = req.body.contestants;
    const win_points: number = req.body.win_points;
    const tie_points : number = req.body.tie_points;
    const defeat_points : number = req.body.defeat_points;

    const contestants: string[] = contestants_text.split('\r\n');

    const user_id: string = req.oidc.user?.sub;
    const user_name: string = req.oidc.user?.name;
    const user_email: string = req.oidc.user?.email;

    const tournament_id = uuidv4();

    try {
        const user_ids = await db.getUserIds();

        if(!user_ids.includes(user_id)) {
            await db.insertUser(user_id, user_name, user_email);
        }

        await db.insertNewTournament(tournament_id, name, user_id, win_points, tie_points, defeat_points);

        await db.insertPlayers(contestants, tournament_id);

    } catch(err) {
        console.log(err);
    }

    res.redirect(`/my-tournaments/`)
});

app.get('/tournament/:tournament_id', async function(req, res) {

    const tournament_id = req.params.tournament_id;

    let tournament: db.Tournament;
    try {
        tournament = await db.getTournamentInfo(tournament_id);
    } catch(err) {
        res.status(400).end(err.message)
        return;
    }

    const players = await db.getTournamentPlayers(tournament_id);

    const games = await db.getTournamentGames(tournament_id);

    const gameSchedules = await db.getTournamentSchedule(players.length);

    // check if the user should be able to edit tournament data
    let isAuthorized: boolean = false;
    if(req.oidc.isAuthenticated()) {
        let username: string = req.oidc.user?.sub

        if(username === tournament.owner_id) {
            isAuthorized = true;
        }
    }

    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

    let standings: db.StandingsEntry[] = [];

    for(const player of players) {
        const player_games = await db.getPlayerGames(player.player_id)

        let played_games = player_games.length

        const p1_games = player_games.filter(g => {
            return g.player1_id === player.player_id
        })

        const p2_games = player_games.filter(g => {
            return g.player2_id === player.player_id
        })

        let wins = p1_games.filter(g => g.score1 > g.score2).length + p2_games.filter(g => g.score2 > g.score1).length
        let ties = player_games.filter(g => g.score1 === g.score2).length
        let defeats = played_games - wins - ties

        let points = (tournament.win_points * wins) + (tournament.tie_points * ties) + (tournament.defeat_points * defeats)

        standings.push({
            player: player,
            wins: wins,
            ties: ties,
            defeats: defeats,
            played: played_games,
            points: points
        })
    }
    
    standings.sort((a, b) => {
        return b.points - a.points
    })

    res.render('tournament', {isAuthorized, tournament, players, games, gameSchedules, fullUrl, standings});
});

app.post('/tournament/:tournament_id', requiresAuth(), async function(req, res) {

    const tournament_id = req.params.tournament_id;
    const tournament_info = await db.getTournamentInfo(tournament_id);

    if(req.oidc.user?.sub !== tournament_info.owner_id) {
        res.status(401).end('Unauthorized!')
    }

    const games = await db.getTournamentGames(tournament_id);
    const players = await db.getTournamentPlayers(tournament_id);
    const gameSchedules = await db.getTournamentSchedule(players.length);

    // update score for already existing games
    for(const game of games) {
        let id1 = `${game.game_id}-score1`
        let id2 = `${game.game_id}-score2`

        let newScore1 = req.body[id1]
        let newScore2 = req.body[id2]

        // check if there are actual values being sent
        if(newScore1 !== '' && newScore2 !== '') {
            if(newScore1 !== game.score1 || newScore2 !== game.score2) { // check if scores were updated
                await db.updateGameScore(game.game_id, newScore1, newScore2)
            }
        } else { // if empty values are received - delete game score
            await db.deleteGame(game.game_id)
        }
    }

    // get schedules for the games that were not yet played
    let played_games_order_numbers = games.map(g => g.game_order_number)
    let unplayed_games_schedules = gameSchedules.filter(schedule => {
        return !played_games_order_numbers.includes(schedule.order_number)
    })

    let players_numbers = players.map(p => p.player_number)

    // insert score for new games, if the score has been set
    console.log(unplayed_games_schedules)
    for(const schedule of unplayed_games_schedules) {
        let game_order_number = schedule.order_number

        let id1 = `${game_order_number}-score1`
        let id2 = `${game_order_number}-score2`

        let newScore1 = req.body[id1]
        let newScore2 = req.body[id2] 
        
        // the score data has been set for the game
        if(newScore1 !== '' && newScore2 !== '') {

            let player1_index = players_numbers.indexOf(schedule.player1_number);
            let player2_index = players_numbers.indexOf(schedule.player2_number);

            if(player1_index !== undefined && player2_index !== undefined) {
                let player1_id = players[player1_index].player_id;
                let player2_id = players[player2_index].player_id;

                if(player1_id !== undefined && player2_id !== undefined) {
                    await db.insertGame(tournament_id, player1_id, player2_id, schedule.round, schedule.order_number, newScore1, newScore2);
                }
            }
        }
    }

    console.log(req.body)

    res.redirect(`/tournament/${tournament_id}`)
});

// start server
if (externalUrl) {
    const hostname = '0.0.0.0';
    app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
    });

} else {
    https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
        console.log(`Server running at https://localhost:${port}/`);
    });
}